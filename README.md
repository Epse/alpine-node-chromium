# alpine-node-chromium

A very basic Docker image containing Chromium and node
Currently only supports a limited set of tags.

The `CHROME_BIN` env var is exposed, pointing to where chromium is located.
By default, this tracks `node:alpine`, which is the latest version of node available on the alpine platform.

## License

Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)
at your option.
